package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/feeds"
)

func main() {
	listenAddr := os.Getenv("LISTEN_ADDR")
	if listenAddr == "" {
		listenAddr = ":9595"
	}
	twitterConsumerKey := os.Getenv("TWITTER_CONSUMER_KEY")
	twitterConsumerSecret := os.Getenv("TWITTER_CONSUMER_SECRET")
	twitterToken := os.Getenv("TWITTER_APP_TOKEN")
	twitterTokenSecret := os.Getenv("TWITTER_APP_TOKEN_SECRET")

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	defer cancelAppCtx()

	config := oauth1.NewConfig(twitterConsumerKey, twitterConsumerSecret)
	token := oauth1.NewToken(twitterToken, twitterTokenSecret)
	httpClient := config.Client(appCtx, token)

	// setup timeouts
	httpClient.Timeout = 10 * time.Second
	oathTr, ok := httpClient.Transport.(*oauth1.Transport)
	if !ok {
		log.Fatalf("failed to convert roundtripper into oauth1.Transport")
	}
	oathTr.Base = &http.Transport{
		MaxIdleConns:          20,
		IdleConnTimeout:       10 * time.Second,
		ResponseHeaderTimeout: 2 * time.Second,
	}
	httpClient.Transport = oathTr

	twClient := twitter.NewClient(httpClient)

	r := chi.NewRouter()
	r.Use(middleware.Recoverer)

	r.Route("/api/v1", func(r chi.Router) {
		r.Get("/rss/{twitterName}", twitterFuncHandler(twClient))
	})

	server := &http.Server{
		Addr: listenAddr,
		BaseContext: func(net.Listener) context.Context {
			return appCtx
		},
		ReadHeaderTimeout: 10 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       10 * time.Second,
		Handler:           r,
	}

	log.Println("started listening on:", listenAddr)
	signals := make(chan os.Signal)
	signal.Notify(signals, os.Interrupt, os.Kill)

	go func() {
		<-signals
		cancelAppCtx()

		ctx, cancelCtx := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelCtx()
		if err := server.Shutdown(ctx); err != nil {
			log.Printf("error shutting down server: %v", err)
		}
	}()

	err := server.ListenAndServe()
	if err != nil {
		log.Printf("error while starting server: %v", err)
	}
}

func twitterFuncHandler(
	twClient *twitter.Client,
) http.HandlerFunc {
	twitterAccountURLTmpl := "https://twitter.com/%s"
	twitterStatusURLTmpl := twitterAccountURLTmpl + "/status/%s"

	return func(w http.ResponseWriter, r *http.Request) {
		twitterName := chi.URLParam(r, "twitterName")
		if twitterName == "" {
			http.Error(w, "no twitter user name provided", http.StatusBadRequest)
			return
		}

		twitterUser, _, err := twClient.Users.Show(
			&twitter.UserShowParams{
				ScreenName: twitterName,
			},
		)
		if err != nil {
			log.Printf("failed to fetch user data: %v", err)
			http.Error(w, "failed to fetch user data", http.StatusInternalServerError)
			return
		}

		timelineLimit := 20
		reqTimelineLimit, _ := strconv.Atoi(r.URL.Query().Get("limit"))
		if reqTimelineLimit > 0 && reqTimelineLimit <= 100 {
			timelineLimit = reqTimelineLimit
		}

		twitterUserTimeline, _, err := twClient.Timelines.UserTimeline(
			&twitter.UserTimelineParams{
				ExcludeReplies:  twitter.Bool(true),
				IncludeRetweets: twitter.Bool(false),
				ScreenName:      twitterName,
				Count:           timelineLimit,
				// so we get the full tweet text, we set TweetMode to "extended"
				TweetMode: "extended",
			},
		)
		if err != nil {
			log.Printf("failed to fetch user timeline data: %v", err)
			http.Error(w, "failed to fetch user timeline data", http.StatusInternalServerError)
			return
		}

		now := time.Now()
		authorName := twitterName
		if twitterUser.Name != "" {
			authorName = twitterUser.Name
		}
		author := &feeds.Author{Name: authorName, Email: twitterUser.Email}

		twFeed := &feeds.Feed{
			Title:       authorName + "s' twitter feed",
			Subtitle:    twitterUser.URL,
			Description: twitterUser.Description,
			Author:      author,
			Created:     now,
			Updated:     now,
			Image: &feeds.Image{
				Url: twitterUser.ProfileImageURLHttps,
			},
			Link: &feeds.Link{
				Href: fmt.Sprintf(twitterAccountURLTmpl, twitterUser.ScreenName),
			},
		}

		items := make([]*feeds.Item, len(twitterUserTimeline))
		for i, tw := range twitterUserTimeline {
			created, err := tw.CreatedAtTime()
			if err != nil {
				created = now
			}

			text := tw.FullText
			divider := 1
			if nd := len(text) / 50; nd > 0 {
				divider = nd
			}
			title := text[:len(text)/divider] + "..."

			items[i] = &feeds.Item{
				Title:       title,
				Description: text,
				Author:      author,
				Created:     created,
				Link: &feeds.Link{
					Href: fmt.Sprintf(
						twitterStatusURLTmpl,
						twitterUser.ScreenName,
						tw.IDStr,
					),
				},
			}
		}

		twFeed.Items = items
		if len(items) > 0 {
			twFeed.Updated = items[0].Created
		}

		w.Header().Add("Content-Type", "application/rss+xml; charset=utf-8")
		w.Header().Add("Content-Type", "text/xml")
		twFeed.WriteRss(w)
	}
}
