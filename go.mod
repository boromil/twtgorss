module gitlab.com/boromil/twtgorss

go 1.14

require (
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/dghubble/oauth1 v0.6.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/gorilla/feeds v1.1.1
	github.com/kr/pretty v0.2.0 // indirect
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
)
